import { Directive, Input } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, ValidationErrors, Validator, Validators } from '@angular/forms';

const RANGE_VALIDADOR = 'faixa';
@Directive({
  selector: '[min][max]',
  providers: [{ provide: NG_VALIDATORS, useExisting: RangeValidatorDirective, multi: true }]
})
export class RangeValidatorDirective implements Validator {
  @Input() min: number | undefined;
  @Input() max: number | undefined;

  constructor() { }

  validate(control: AbstractControl): ValidationErrors | null {

    if (control.value) {
      if (this.min && control.value < this.min) {
        const minError = {
          min: this.min,
          atual: control.value
        }
        return {
          [RANGE_VALIDADOR]: minError
        }

        // ou em ES5
        // const error: any = {};
        // error[RANGE_VALIDADOR] =  minError;
        // return error;
      }

      if (this.max && control.value > this.max) {
        return {
          [RANGE_VALIDADOR]: {
            max: this.max,
            atual: control.value
          }
        }
      }

    }

    return null;

    // const minValidatorFn = Validators.min(this.min);
    // const resultadoMinValidatorFn = minValidatorFn(control);
    // return {
    //   ...resultadoMinValidatorFn,
    //   ...Validators.max(this.max)(control)
    // };
  }

}
