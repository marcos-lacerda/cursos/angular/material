import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'app-edit-validation-reactive',
  templateUrl: './validation.component.html',
  styleUrls: ['./validation.component.css']
})
export class ReactiveEditValidationComponent implements OnInit {

  form: FormGroup;

  nome: FormControl;
  salario: FormControl;

  constructor(
    private formBuilder: FormBuilder,
  ) {
    this.nome = this.formBuilder.control('', [Validators.required])
    this.salario = this.formBuilder.control('',
        [Validators.required,
        Validators.max(5000),
        Validators.min(1000)])

      this.form = this.formBuilder.group({
      nome: this.nome,
      salario: this.salario,
    });
  }

  ngOnInit(): void {}

  submit(): void {
    console.log(this.form.status);
    console.log(this.form.value);
  }

}
