# Exercício 04

## Objetivos

* Fixar nosso conhecimento dos elementos básicos do angular
  * Eventos

## Requisitos

- complementar o exercicio 3
- o componente deve ter um campo de saida (emitir um evento) sempre que o valor do contador sofrer alteração.
- o serviço não deve aceitar o valor 33 para o valor tanto na alteração como no incremento, o usuário deve ser notificado sobre esse valor inválido.
- sempre que o contador sofra alteração, o serviço deve emitir um evento informando o novo valor

## Resposta

### deixe a aplicação executando

```
npm start
```

