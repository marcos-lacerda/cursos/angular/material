# Angular Básico

![Angular](img/angular.png)

## [Acessar Slides](slides/index.html)

## Frontend - Angular
* Angular: o que é
* Arquiteturas Web Modernas
* nvm/node.js/npm/Angular CLI
* Módulos
* Componentes
* Templates
* Metadata
* Data Binding
* Diretivas
* Pipes
* Serviços
* Rotas
* Guardas
* RxJs

## Exercícios:
* [01 - Instalação](exercicios/exercicio-01.md)
* [02 - Primeira Aplicação Angular](exercicios/exercicio-02.md)
* [03 - Componentes, Diretivas e Serviços](exercicios/exercicio-03.md)
* [04 - RxJs](exercicios/exercicio-04.md)
